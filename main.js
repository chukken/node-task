const express = require("express");
const app = express();
const PORT = 3000;

//Base url: http://localhost:3000
// Endpoint: https://localhost.3000/
app.get('/', (req, res) => {
    res.send("Hello World");
});


/**
 * This is arrow function to add two numbers together
 * @param {Number} a is the first number
 * @param {Number} b is the second number
 * @return {Number} This is the sum of the a and b params.
 */
const add = (a, b) =>{
    const sum = a + b;
    return sum;
};

app.use(express.json());
app.post("/add", (req, res) => {
    const a = req.body.a;
    const b = req.body.b;
    const sum = add(a, b);
    res.send(sum.toString());
});


//Server Start
app.listen(PORT, () => console.log(
    `Server listening at http://localhost:${PORT}`
));


console.log("Express application starting..")